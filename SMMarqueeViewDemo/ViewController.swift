//
//  ViewController.swift
//  SMMarqueeViewDemo
//
//  Created by a on 2022/6/14.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var startButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 150, y: 320, width: 80, height: 44))
        button.setTitle("开始", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.tag = 1
        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var pauseButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 150, y: 390, width: 80, height: 44))
        button.setTitle("暂停", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.tag = 2
        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    private lazy var stopButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 150, y: 450, width: 80, height: 44))
        button.setTitle("结束", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.tag = 3
        button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    private let datas = ["网络用语", "早晚安心语", "游戏世界", "知乎豆瓣高级情话连篇来了", "但实测发现", "藏头诗藏不住蜜糖般的笑容", "发给对象的小情话", "向喜欢的人微信告白", "520遍我爱你在心口难…","向喜欢的人微信告白","近年的梗", "睡前魔性刷屏文字很长很长很长很长…","网络用语", "早晚安心语", "游戏世界", "要讲武德马保国", "微信系统客服"]
    
    private var textList: [String] = []
    private var textList1: [String] = []
    
    private var containerView = UIView(frame: CGRect(x: 0, y: 200, width: UIScreen.main.bounds.width, height: 90))
    private lazy var marqueeView = SMMarqueeView(frame: CGRect(x: 0, y: 210, width: UIScreen.main.bounds.width, height: 28))
    private lazy var marqueeView1 = SMMarqueeView(frame: CGRect(x: 0, y: 210 + 28 + 14, width: UIScreen.main.bounds.width, height: 28))
    
    
//    private let marqueeView2 = GFMarqueeView(frame: CGRect(x: 0, y: 210 + 28 + 14, width: UIScreen.main.bounds.width, height: 28))

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        containerView.backgroundColor = UIColor.lightGray
        
        view.addSubview(containerView)
        view.addSubview(marqueeView)
        view.addSubview(marqueeView1)
        
        view.addSubview(startButton)
        view.addSubview(pauseButton)
        view.addSubview(stopButton)
        
        for i in 0..<datas.count {
            if i % 2 == 0 {
                textList.append(datas[i])
            }else {
                textList1.append(datas[i])
            }
        }
        marqueeView.textList = textList
        marqueeView1.textList = textList1
        
        marqueeView.didClikedAction = { [weak self] index in
            print("marqueeView点击下标：\(index)--\(String(describing: self?.textList[index]))")
        }
        
        marqueeView1.didClikedAction = { [weak self] index in
            print("marqueeView1点击下标：\(index)--\(String(describing: self?.textList1[index]))")
        }
        
//        marqueeView2.textScrollSpeed = 80;
//        marqueeView2.textList = ["母亲的爱涵养家风，家风影响世风。每个家庭前进的脚步，终将叠加成国家的进步；每个家庭创造的价值，定能汇聚成中华民族伟大复兴的力量。","“慈母手中线，游子身上衣。临行密密缝，意恐迟迟归。谁言寸草心，报得三春晖。”2015年的春节团拜会，习近平总书记吟诵唐代诗人孟郊的《游子吟》，道出了他与母亲的深厚感情", "家庭是人生的第一课堂，父母是孩子的第一任老师。党的十八大以来，习近平总书记在多个场合强调家庭家教家风建设的重要性。"];
//        marqueeView2.frame = CGRect(x: 0, y: 180, width: self.view.frame.size.width, height: 30);
//        marqueeView2.backgroundColor = .green;
//        marqueeView2.run();
//        marqueeView2.didSelectedIndexBlock = { (index) in
//
//            print("index-------------------\(index)")
//
//        }

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        run()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        stop()
    }
    
    @objc func buttonAction(sender: UIButton) {
        switch sender.tag {
        case 1:
            run()
        case 2:
            pause()
        case 3:
            stop()
        default:
            fatalError()
        }
    }
    
    private func run() {
        marqueeView.run()
        marqueeView1.run()
    }
    
    private func stop() {
        marqueeView.stop()
        marqueeView1.stop()
    }
    
    private func pause() {
        marqueeView.pause()
        marqueeView1.pause()
    }
}
