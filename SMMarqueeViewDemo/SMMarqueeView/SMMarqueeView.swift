//
//  SMMarqueeView.swift
//  SMMarqueeView
//
//  Created by a on 2022/6/14.
//

import UIKit

class SMMarqueeView: UIView {
    
    // 跑马灯的滚动状态
    enum MarqueeState {
        case running
        case paused
        case stopped
    }
    
    var isRunning: Bool { return state == .running }
    var isPaused: Bool { return state == .paused }
    var isStopped: Bool { return state == .stopped }
    
    private(set) var state: MarqueeState = .stopped
    /// label间距
    private let textSpacing: CGFloat = 10
    /// 滚动速度，默认 50 pt/s
    private let textScrollSpeed: CGFloat = 50
    
    /// 文本列表
    var textList = [String]() {
        willSet { stop() }
        didSet { resetIndex() }
    }
    
    private var nextIndex = NSNotFound
    private var displayLink: CADisplayLink?
    private let marqueeLabelContainerView = UIView()
    private var onScreenMarqueeLabels = [SMMarqueeLabel]()
    private var offScreenMarqueeLabels = [SMMarqueeLabel]()
    
    var didClikedAction: ((Int) -> Void)?
    
    deinit {
        invalidateDisplayLink()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(marqueeLabelContainerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


// MARK: - 滚动控制

extension SMMarqueeView {
    
    // 开始滚动
    func run() {
        guard !isRunning else { return }
        guard !textList.isEmpty else { return }
        
        if state == .stopped {
            addOnScreenMarqueeLabel()
        }
        
        if window != nil {
            resumeDisplayLink()
        }
        state = .running
    }
    
    // 暂停滚动
    func pause() {
        guard isRunning else { return }
        pauseDisplayLink()
        state = .paused
    }
    
    // 停止滚动
    func stop() {
        guard !isPaused else { return }
        resetIndex()
        pauseDisplayLink()
        clearOnScreenMarqueeLabels()
        resetContainerViewBounds()
        state = .stopped
    }
    
}


// MARK: - 布局

extension SMMarqueeView {
    
    override var bounds: CGRect {
        didSet {
            guard bounds != oldValue else { return }
            layoutSubviews2()
        }
    }
    
    override var frame: CGRect {
        didSet {
            guard frame != oldValue else { return }
            layoutSubviews2()
        }
    }
    
    private func layoutSubviews2() {
        marqueeLabelContainerView.frame = bounds
    }
    
    private func resetContainerViewBounds() {
        marqueeLabelContainerView.bounds.origin = .zero
    }
    
}


// MARK: - 更新索引

private extension SMMarqueeView {
    
    func increaseIndex() {
        nextIndex = (nextIndex + 1) % textList.count
    }
    
    func resetIndex() {
        nextIndex = textList.isEmpty ? NSNotFound : 0
    }
    
}


// MARK: - 循环利用

private extension SMMarqueeView {
    
    func dequeueReusableMarqueeLabel() -> SMMarqueeLabel {
        if let marqueeLabel = offScreenMarqueeLabels.popLast() {
            return marqueeLabel
        }
        
        let marqueeLabel = SMMarqueeLabel()
        marqueeLabel.randomGradient()
        return marqueeLabel
    }
    
    func recycle(_ marqueeLabel: SMMarqueeLabel) {
        offScreenMarqueeLabels.append(marqueeLabel)
    }
    
}


// MARK: - 添加移除标签

private extension SMMarqueeView {
    
    func addOnScreenMarqueeLabel() {
        let currentIndex = nextIndex
        increaseIndex()
        let height = frame.height > 0 ? frame.height : 28
        
        let marqueeLabel = dequeueReusableMarqueeLabel()
        marqueeLabel.tag = currentIndex
        marqueeLabel.gradientLayer.cornerRadius = height / 2
        marqueeLabel.gradientLayer.masksToBounds = true
        marqueeLabel.textLabel.text = textList[currentIndex]
        marqueeLabel.textLabel.sizeToFit()
        marqueeLabel.frame.size.height = height
        marqueeLabel.frame.size.width = 2 * marqueeLabel.widthSpacing + marqueeLabel.textLabel.frame.width
        if let lastLabelFrame = onScreenMarqueeLabels.last?.frame {
            marqueeLabel.frame.origin.x = lastLabelFrame.maxX + textSpacing
        } else {
            marqueeLabel.frame.origin.x = marqueeLabelContainerView.bounds.maxX
        }
        
        marqueeLabel.isUserInteractionEnabled = true
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        marqueeLabel.addGestureRecognizer(tapGR)
        
        onScreenMarqueeLabels.append(marqueeLabel)
        marqueeLabelContainerView.addSubview(marqueeLabel)
    }
    
    func clearOnScreenMarqueeLabels() {
        onScreenMarqueeLabels.forEach {
            $0.removeFromSuperview()
            recycle($0)
        }
        onScreenMarqueeLabels.removeAll(keepingCapacity: true)
    }
    
    func removeOffScreenMarqueeLabel() {
        let marqueeLabel = onScreenMarqueeLabels.removeFirst()
        marqueeLabel.removeFromSuperview()
        recycle(marqueeLabel)
    }
    
    @objc private func tapAction(sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag else { return }
        didClikedAction?(tag)
    }
}


// MARK: - 定时器

private extension SMMarqueeView {
    
    func invalidateDisplayLink() {
        displayLink?.invalidate()
        displayLink = nil
    }
    
    func resumeDisplayLink() {
        if displayLink == nil {
            let target = YYWeakProxy(target: self)
            let displayLink = CADisplayLink(target: target, selector: #selector(step))
            displayLink.add(to: .main, forMode: .common)
            self.displayLink = displayLink
        }
        displayLink?.isPaused = false
    }
    
    func pauseDisplayLink() {
        displayLink?.isPaused = true
    }
    
    @objc private func step(_ displayLink: CADisplayLink) {
        let originXOffset = textScrollSpeed * CGFloat(displayLink.duration)
        marqueeLabelContainerView.bounds.origin.x += originXOffset
        if let firstLabel = onScreenMarqueeLabels.first, firstLabel.frame.maxX <= marqueeLabelContainerView.bounds.minX {
            removeOffScreenMarqueeLabel()
        }
        
        if let lastLabelMaxX = onScreenMarqueeLabels.last?.frame.maxX, marqueeLabelContainerView.bounds.maxX - lastLabelMaxX >= textSpacing {
            addOnScreenMarqueeLabel()
        }
    }
    
}


// MARK: - 视图关系变更

extension SMMarqueeView {
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            pauseDisplayLink()
        } else if isRunning {
            resumeDisplayLink()
        }
    }
    
}
