//
//  GFMarqueeView.h
//  SMMarqueeViewDemo
//
//  Created by joedd on 2023/5/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
// 跑马灯的滚动状态
typedef NS_ENUM(NSInteger, MarqueeState) {
    MarqueeStateRunning = 0,
    MarqueeStatePaused,
    MarqueeStateStopped,
};

typedef void(^DidSelectedIndexBlock)(NSInteger);

@interface GFMarqueeView : UIView
/// label间距
@property(nonatomic,assign) CGFloat textSpacing;
/// 滚动速度，默认 50 pt/s
@property(nonatomic,assign) CGFloat textScrollSpeed;
/// 文本数据
@property(nonatomic,strong) NSArray<NSString *> *textList;
/// 选中回调
@property(nonatomic, copy) DidSelectedIndexBlock didSelectedIndexBlock;
// 开始滚动
-(void)run;
// 暂停滚动
-(void)pause;
// 停止滚动
-(void)stop;

@end

NS_ASSUME_NONNULL_END
