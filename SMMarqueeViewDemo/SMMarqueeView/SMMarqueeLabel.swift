//
//  SMMarqueeLabelView.swift
//  SMMarqueeLabelView
//
//  Created by a on 2022/6/14.
//

import UIKit

class SMMarqueeLabel: UIView {
    
    struct GradientItem {
        var icon: String
        var colors: [String]
    }
    
    let imageView = UIImageView()
    let gradientLayer = CAGradientLayer()
    let textLabel = UILabel()
    
    private let gradientColors: [GradientItem] = [GradientItem(icon: "phrase_home_quotationshape0", colors: ["#EFFAFF", "#F6F6FF"]),
                                                  GradientItem(icon: "phrase_home_quotationshape1", colors: ["#F1FFE6", "#ECFFF7"]),
                                                  GradientItem(icon: "phrase_home_quotationshape2", colors: ["#F7F2FF", "#FFF2FB"]),
                                                  GradientItem(icon: "phrase_home_quotationshape3", colors: ["#FFF3EF", "#FFF7E7"]),
                                                  GradientItem(icon: "phrase_home_quotationshape4", colors: ["#FFEEEE", "#FFF1FA"]),
                                                  GradientItem(icon: "phrase_home_quotationshape5", colors: ["#E2FFF9", "#EEF6FF"]),
                                                  GradientItem(icon: "phrase_home_quotationshape6", colors: ["#FFF1EA", "#FFECF0"]),
                                                  GradientItem(icon: "phrase_home_quotationshape7", colors: ["#E6FBFF", "#F3FFEC"])]
    /// 宽度增加
    let widthSpacing: CGFloat = 14
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint.zero
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        self.layer.addSublayer(gradientLayer)
        
        textLabel.textColor = UIColor(hex: "#6F6F7E")
        textLabel.font = UIFont.systemFont(ofSize: 13)
        textLabel.textAlignment = .center
        
        addSubview(imageView)
        addSubview(textLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = CGRect(origin: .zero, size: self.bounds.size)
        textLabel.frame = CGRect(x: widthSpacing, y: 0, width: self.bounds.size.width - 2 * widthSpacing, height: self.bounds.size.height)
        imageView.frame = CGRect(x: 0, y: -2, width: 10, height: 8.5)
    }
    
    func randomGradient() {
        if let item = gradientColors.randomElement() {
            let colors = item.colors
            if colors.count == 2 {
                self.gradientLayer.colors = [UIColor(hex: colors[0])?.cgColor ?? UIColor.red.cgColor, UIColor(hex: colors[1])?.cgColor ?? UIColor.red.cgColor]
            }
            self.imageView.image = UIImage(named: item.icon)
        }
    }
    
}
